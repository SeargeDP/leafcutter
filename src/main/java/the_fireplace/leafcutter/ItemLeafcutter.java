package the_fireplace.leafcutter;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockWeb;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemLeafcutter extends Item {
	public ItemLeafcutter(){
		setUnlocalizedName("leafcutter");
		setCreativeTab(CreativeTabs.tabTools);
		setMaxDamage(1024);
		setMaxStackSize(1);
	}
	@Override
	public boolean hitEntity(ItemStack is, EntityLivingBase entityHit, EntityLivingBase attacker){
		is.damageItem(2, attacker);
		entityHit.attackEntityFrom(DamageSource.generic, 2);
		return true;
	}
	@Override
	public boolean onBlockDestroyed(ItemStack is, World world, Block block, BlockPos pos, EntityLivingBase entity){
		if((double)block.getBlockHardness(world, pos) != 0){
			is.damageItem(1, entity);
		}
		if(block instanceof BlockLeaves || block instanceof BlockWeb){
			block.dropBlockAsItem(world, pos, world.getBlockState(pos), 5);
			return true;
		}
		return super.onBlockDestroyed(is, world, block, pos, entity);
	}
	@Override
	@SideOnly(Side.CLIENT)
	public boolean isFull3D(){
		return true;
	}
	@Override
	public int getItemEnchantability(){
		return 5;
	}
	@Override
	public float getDigSpeed(ItemStack is, IBlockState state){
		if(state.getBlock() instanceof BlockLeaves || state.getBlock() instanceof BlockWeb){
			return 5000;
		}
		return super.getDigSpeed(is, state);
	}
}